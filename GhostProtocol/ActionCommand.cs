﻿using System;
using System.Windows.Input;

namespace GhostProtocol
{
    public class ActionCommand : ICommand
    {
        private Action executeAction;
        private Func<bool> canExecuteMethod;

        public ActionCommand(Action executeAction)
        {
            ExecuteAction = executeAction;
        }

        public ActionCommand(Action executeAction, Func<bool> canExecuteMethod)
        {
            ExecuteAction = executeAction;
            this.canExecuteMethod = canExecuteMethod;
        }

        public Action ExecuteAction { get => executeAction; set => executeAction = value; }

        public event EventHandler CanExecuteChanged = delegate { };

        public bool CanExecute(object parameter)
        {
            if (canExecuteMethod != null)
                return canExecuteMethod();
            if (ExecuteAction != null)
                return true;
            return false;
        }

        public void Execute(object parameter)
        {
            ExecuteAction?.Invoke();
        }

        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged(this, EventArgs.Empty);
        }
    }
}
