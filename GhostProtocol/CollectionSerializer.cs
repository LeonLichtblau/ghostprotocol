﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;
using GhostProtocol.Model;

namespace GhostProtocol
{
    public class CollectionSerializer
    {
        public static void Serialize(string path, ObservableCollection<ProtocolEntry> entryCollection)
        {
            var str = new StringBuilder();
            foreach (var protocolEntry in entryCollection)
            {
                str.AppendLine($"#{protocolEntry.Timestamp}|{protocolEntry.Content}");
            }
            File.WriteAllText(path, str.ToString());
        }

        public static List<ProtocolEntry> Deserialize(string path)
        {
            var entries = new List<ProtocolEntry>();
            using (var fr = new StreamReader(File.OpenRead(path)))
            {
                string[] entryBlocks = fr.ReadToEnd().Split('#');
                foreach (var entryBlock in entryBlocks)
                {
                    if(string.IsNullOrEmpty(entryBlock))
                        continue;
                    string[] entrySplit = entryBlock.Split('|');
                    var timestamp = DateTime.Parse(entrySplit[0]);
                    var content = entrySplit[1];

                    entries.Add(new ProtocolEntry(timestamp, content));
                }
            }
            return entries;
        }
    }
}
