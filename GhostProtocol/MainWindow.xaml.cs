﻿using GhostProtocol.ViewModel;
using System.Collections.ObjectModel;
using System.Windows;
using GhostProtocol.Model;

namespace GhostProtocol
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ObservableCollection<ProtocolEntry> EntryCollection { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            EntryCollection = new ObservableCollection<ProtocolEntry>();
            AddEntryView.DataContext = new AddProtocolEntryViewModel(EntryCollection);
            EntryMenu.DataContext = new ProtocolMenuViewControl(EntryCollection);
        }
    }
}
