﻿using System;

namespace GhostProtocol.Model
{
    public class ProtocolEntry : BindableBase
    {
        private DateTime _timestamp;

        public DateTime Timestamp
        {
            get => _timestamp;
            set
            {
                SetProperty(ref _timestamp, value);
                OnPropertyChanged("Timestamp");
            }
        }

        private string _content;

        public string Content
        {
            get => _content;
            set
            {
                SetProperty(ref _content, value);
                OnPropertyChanged("Content");
            }
        }

        public ProtocolEntry(DateTime timestamp, string content)
        {
            Timestamp = timestamp;
            Content = content;
        }
    }
}
