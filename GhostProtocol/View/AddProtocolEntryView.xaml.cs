﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GhostProtocol.View
{
    /// <summary>
    /// Interaction logic for AddProtocolEntryView.xaml
    /// </summary>
    public partial class AddProtocolEntryView : UserControl
    {
        public AddProtocolEntryView()
        {
            InitializeComponent();
        }

        private void ContentBox_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                AddButton.Command.Execute(null);
        }
    }
}
