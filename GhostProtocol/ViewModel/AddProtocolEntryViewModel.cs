﻿using GhostProtocol.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GhostProtocol.ViewModel
{
    public class AddProtocolEntryViewModel : BindableBase
    {
        private string _currentContent;
        public string CurrentContent
        {
            get => _currentContent;
            set
            {
                SetProperty(ref _currentContent, value);
                AddCommand.RaiseCanExecuteChanged();
            }
        }
        public ActionCommand AddCommand { get; set; }

        public ObservableCollection<ProtocolEntry> EntryCollection { get; set; }

        public AddProtocolEntryViewModel(ObservableCollection<ProtocolEntry> entryCollection)
        {
            EntryCollection = entryCollection;
            AddCommand = new ActionCommand(OnAdd, CanAdd);
        }

        public void OnAdd()
        {
            var entry = new ProtocolEntry(DateTime.Now, CurrentContent);
            EntryCollection.Add(entry);
            CurrentContent = string.Empty;
        }

        private bool CanAdd()
        {
            return !string.IsNullOrEmpty(CurrentContent);
        }
    }
}
