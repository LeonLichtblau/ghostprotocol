﻿using GhostProtocol.Model;

namespace GhostProtocol.ViewModel
{
    public class EditWindowViewModel
    {
        public ProtocolEntry SelectedEntry { get; set; }
        public EditWindowViewModel(ProtocolEntry entry)
        {
            SelectedEntry = entry;
        }
    }
}