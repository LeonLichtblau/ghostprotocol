﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows.Automation.Peers;
using GhostProtocol.Model;
using GhostProtocol.View;
using Microsoft.Win32;

namespace GhostProtocol.ViewModel
{
    public class ProtocolMenuViewControl : BindableBase
    {
        public ActionCommand LoadCommand { get; set; }
        public ActionCommand SaveCommand { get; set; }
        public ActionCommand DeleteCommand { get; set; }
        public ActionCommand EditCommand { get; set; }

        public ObservableCollection<ProtocolEntry> EntryCollection { get; set; }

        private ProtocolEntry _currentSelectedEntry;
        public ProtocolEntry CurrentSelectedEntry
        {
            get => _currentSelectedEntry;
            set
            {
                SetProperty(ref _currentSelectedEntry, value);
                DeleteCommand.RaiseCanExecuteChanged();
                EditCommand.RaiseCanExecuteChanged();
            }
        }


        public ProtocolMenuViewControl(ObservableCollection<ProtocolEntry> entryCollection)
        {
            EntryCollection = entryCollection;
            LoadCommand = new ActionCommand(OnLoad);
            SaveCommand = new ActionCommand(OnSave);
            DeleteCommand = new ActionCommand(OnDelete, CanEdit);
            EditCommand = new ActionCommand(OnEdit, CanEdit);
        }

        private bool CanEdit()
        {
            return CurrentSelectedEntry != null;
        }

        private void OnDelete()
        {
            EntryCollection.Remove(CurrentSelectedEntry);
        }

        private void OnEdit()
        {
            var window = new EditWindow()
            {
                DataContext = new EditWindowViewModel(CurrentSelectedEntry)
            };
            window.Show();
        }

        private void OnLoad()
        {
            var dialog = new OpenFileDialog()
            {
                Filter = "Protocol File (*.protocol)|*.protocol"
            };
            if (dialog.ShowDialog() == true)
            {
                EntryCollection.Clear();
                foreach (var protocolEntry in CollectionSerializer.Deserialize(dialog.FileName))
                {
                    EntryCollection.Add(protocolEntry);
                }
            }
        }

        private void OnSave()
        {
            var dialog = new SaveFileDialog
            {
                Filter = "Protocol File (*.protocol)|*.protocol",
                AddExtension = true,
                DefaultExt = "protocol"
            };
            if (dialog.ShowDialog() == true)
            {
                CollectionSerializer.Serialize(dialog.FileName, EntryCollection);
            }
        }
    }
}